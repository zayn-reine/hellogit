#include <iostream>
#include <vector> //to use vector type

using namespace std;

int main()
{
	
		vector <char> vcomp;
		vector <int> v1 = {10,14,32,64,16};
		
		vcomp.push_back('k');  //add new element to the back of array
		v1.pop_back(); //remove last element from the array
		
		//Printing Vectors
		
		//Option 1
		
		for(int i = 0; i < v1.size(); i++)
			
			cout << v1 [i] << ",";
			
			
			
		//Option 2
		
		for (auto v : vcomp) //for each value in v1
			
			cout << v << "," <<endl;
			
		return 0;
		
		}
		
